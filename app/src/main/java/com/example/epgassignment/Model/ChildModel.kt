package com.example.epgassignment.Model;

data class ChildModel(
        val image : Int = -1,
        val title : String = ""
)