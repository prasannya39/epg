package com.example.epgassignment.Model;

import android.util.Log
import com.example.epgassignment.R
import java.text.SimpleDateFormat
import java.util.*

object ParentDataFactory{
    private val random = Random()

    private val titles =  arrayListOf( "Zee", "sony", "setMax", "Star Movies", "MTV", "ESPN", "POGO")

    private fun randomTitle() : String{
        val index = random.nextInt(titles.size)
        return titles[index]
    }

    private fun randomChildren() : List<ChildModel>{
        return ChildDataFactory.getChildren(20)
    }

    fun getParents(count : Int) : List<ParentModel>{
        val parents = mutableListOf<ParentModel>()
        repeat(count){
            val parent = ParentModel(randomTitle(),getRandomImage(), randomChildren())
            parents.add(parent)
        }
        return parents
    }

    private fun getRandomImage(): Int {
        return R.drawable.setmax

    }


}