package com.example.epgassignment.Model;

data class ParentModel (
        val title : String = "",
        val logo : Int = -1,
        val children : List<ChildModel>
)