package com.example.epgassignment.views.adapters

import android.graphics.Color
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.example.epgassignment.Model.ChildDataFactory
import com.example.epgassignment.Model.ChildModel
import com.example.epgassignment.Model.ParentModel
import com.example.epgassignment.R
import kotlinx.android.synthetic.main.parent_recycler.view.*
import java.text.SimpleDateFormat
import java.util.*


class ParentAdapter(private val parents : List<ParentModel>, val currentPosition : Int ,val isInitial: Boolean) : RecyclerView.Adapter<ParentAdapter.ViewHolder>(){

    private val viewPool = RecyclerView.RecycledViewPool()
    private val currentPos = currentPosition
    var timeList: MutableList<ChildModel> = mutableListOf<ChildModel>()



    var timeArray= arrayOf("12:00", "1:00","2:00","3:00","4:00","5:00","6:00","7:00","8:00","9:00","10:00","11:00")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.parent_recycler,parent,false)

     for (i in timeArray) {


         val child = ChildModel(0, i)
         timeList.add(child)
     }
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
       return parents.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val parent = parents[position]

        if (position==0){
            holder.textView.text = getDate()
            holder.main.setBackgroundColor(Color.LTGRAY)

            holder.ivLogo.setImageResource(0)
            holder.recyclerView.apply {
                layoutManager = LinearLayoutManager(
                    holder.recyclerView.context,
                    LinearLayout.HORIZONTAL,
                    false
                ) as RecyclerView.LayoutManager?
                adapter = ChildAdapter(timeList,currentPos)
                recycledViewPool = viewPool
            }
        }else {
            holder.textView.text = parent.title
            holder.ivLogo.setImageResource(parent.logo)
            holder.recyclerView.apply {
                layoutManager = LinearLayoutManager(
                    holder.recyclerView.context,
                    LinearLayout.HORIZONTAL,
                    false
                ) as RecyclerView.LayoutManager?
                adapter = ChildAdapter(parent.children,currentPosition)
                recycledViewPool = viewPool
            }
        }


            holder.recyclerView.scrollToPosition(currentPosition)

    }


    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val recyclerView : RecyclerView = itemView.rv_child
        val textView:TextView = itemView.textView
        val ivLogo:ImageView = itemView.ivLogo
        val main:RelativeLayout = itemView.mainRL
    }

    private fun getDate (): String {
        val sdf = SimpleDateFormat("dd MMM YY")
        val currentDate = sdf.format(Date())
        Log.i("Hour",currentDate)

        return currentDate
    }

}