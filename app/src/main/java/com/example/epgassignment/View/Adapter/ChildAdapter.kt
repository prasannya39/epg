package com.example.epgassignment.views.adapters

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.epgassignment.Model.ChildModel
import com.example.epgassignment.R
import kotlinx.android.synthetic.main.child_recycler.view.*


class ChildAdapter(private val children : List<ChildModel>, val currentPosition:Int)
    : RecyclerView.Adapter<ChildAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v =  LayoutInflater.from(parent.context)
                      .inflate(R.layout.child_recycler,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return children.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val child = children[position]
       // holder.imageView.setImageResource(child.image)
        holder.textView.text = child.title

        if (position==currentPosition){

            Log.i("prasannJeet",position.toString()+"--"+ currentPosition)
            holder.textView.setBackgroundColor(Color.LTGRAY)
            holder.textView.setTextColor(Color.BLACK)
        }else{
            holder.textView.setBackgroundColor(Color.GRAY)
            holder.textView.setTextColor(Color.WHITE)

        }

       /* holder.itemView.setOnClickListener {
            Log.i("Prasann",position.toString());
        }*/
    }


    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){

        val textView : TextView = itemView.child_textView
       // val imageView: ImageView = itemView.child_imageView

    }
}