package com.example.epgassignment.View

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Button
import android.widget.LinearLayout
import com.example.epgassignment.Model.ParentDataFactory
import com.example.epgassignment.R
import com.example.epgassignment.views.adapters.ParentAdapter

import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    lateinit var recyclerView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRecycler(getcrrentTime(),true)

        // get reference to button
        val btn_click_me = findViewById(R.id.fab) as FloatingActionButton
        // set on-click listener
        btn_click_me.setOnClickListener {
            // your code to perform when the user clicks on the button
            initRecycler(getcrrentTime(),false)
        }

    }

    private fun initRecycler( pos :Int, isInitial: Boolean){
        recyclerView = rv_parent

        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity, LinearLayout.VERTICAL, false)
            adapter = ParentAdapter(ParentDataFactory.getParents(40),pos,isInitial)
        }

    }


    private fun getcrrentTime(): Int {
        val sdf = SimpleDateFormat("hh")
        val currentDate = sdf.format(Date())
        Log.i("Hour",currentDate)
        val currentDateInt = currentDate.toInt()

        return currentDateInt
    }


}